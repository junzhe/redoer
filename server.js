
const opn = require('opn');
const express = require('express');
const path = require('path');
const fs = require('fs');
const app = express();
const port = 3000;

const dataPath = path.join(__dirname, 'data');

var questions = {}

app.use(express.static('dist'));

app.get('/packlist', (req, res) => {
    var packList = [];
    let files = fs.readdirSync(dataPath);
    files.forEach(file => {
        let name = file.toLowerCase();
        if(name.startsWith("questions_") && name.endsWith(".json")){
            packList.push(name.replace("questions_", "").replace(".json", "").toUpperCase());
        }
    });

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(packList));
})

app.get('/questioncount/:suffix', (req, res) => {
    let obj = {
        count:-1
    };

    let name = "questions_" + req.params.suffix.toLowerCase() + ".json";

    if(questions[name] == undefined){
        try {
            let rawdata = fs.readFileSync(path.join(dataPath, name));
            questions[name] = JSON.parse(rawdata);

            obj.count = questions[name].questions.length;
        } catch (error) {
            console.log(error);
        }
    }

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(obj));
})

app.get('/question/:suffix/:id', (req, res) => {
    let question = {};
    
    let name = "questions_" + req.params.suffix.toLowerCase() + ".json";
    let id = req.params.id;

    try {
        question = questions[name].questions[id];
    } catch (error) {
        
    }

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(question));
})

app.listen(port, () => console.log(`Start listening at http://localhost:${port}`))

opn('http://localhost:' + port);