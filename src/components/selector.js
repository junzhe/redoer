
import React from 'react';
import ReactDOM from 'react-dom';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

let URL_PREFIX;

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    URL_PREFIX = "http://localhost:3000"
} else {
    URL_PREFIX = ""
}

export default class Selector extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            options: [],
            selectedOption: 0,
        };

        this._onSelect = this._onSelect.bind(this);
    }

    componentDidMount() {
        fetch(URL_PREFIX + '/packlist',{method: 'get',
        mode: 'no-cors'})
            .then(response => response.json())
            .then(data => {
                this.setState({ options:data, selectedOption:data[0] });
                console.log(this.state)
            });
    }

    _onSelect(e) {
        this.state.selectedOption = e.value;
        console.log(this.state)
    }

    render() {

        return <div>
            <Dropdown options={this.state.options} onChange={this._onSelect} value={this.state.selectedOption} placeholder="Select a question package" />
            <button onClick={this.props.changePack.bind(this, this.state)}>Select</button>
        </div>;
    }
}
