
import React from 'react';
import ReactDOM from 'react-dom';

import Question from "./question";
import Buttons from "./button";

let URL_PREFIX;

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    URL_PREFIX = "http://localhost:3000"
} else {
    URL_PREFIX = ""
}

export default class PackExecuter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            idMax:10
        };

        this.goPrevious = this.goPrevious.bind(this);
        this.goNext = this.goNext.bind(this);
        this.check = this.check.bind(this);
    }

    componentDidMount() {
        fetch(URL_PREFIX + '/questioncount/' + this.props.packSuffix)
            .then(response => response.json())
            .then(data => {
                this.setState({ idMax:data.count });
            });
    }

    goPrevious(){
        if(this.state.id > 0){
            this.setState({id:this.state.id - 1})
        }
    }

    goNext(){
        if(this.state.id < this.state.idMax - 1){
            this.setState({id:this.state.id + 1})
        }
    }

    check(){
        console.log("check question " + this.state.id);
    }

    render() {
        return <div>
            <h1>{this.props.packSuffix} Question No.{this.state.id + 1}</h1>
            <Question packSuffix={this.props.packSuffix} id={this.state.id}/>
            <Buttons onPrevious={this.goPrevious} onNext={this.goNext} onCheck={this.check}/>
        </div>;
    }
}
