
import React from 'react';
import ReactDOM from 'react-dom';

class Buttons extends React.Component {
    render() {
        return <div>
        <button onClick={this.props.onPrevious}>Previous</button>
            <button onClick={this.props.onCheck}>Check</button>
            <button onClick={this.props.onNext}>Next</button>
        </div>
    }
}

export default Buttons;