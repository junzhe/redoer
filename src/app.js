
import React from 'react';
import ReactDOM from 'react-dom';

import Selector from "./components/selector"
import PackExecuter from "./components/packwraper"


export default class App extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            setPack: false
        }
        this.changePack = this.changePack.bind(this)
    }

    changePack(e) {
        this.setState({ packSuffix: e.selectedOption, setPack: true })
    }

    render() {
        return <div>
            {this.state.setPack ? (
                <PackExecuter packSuffix={this.state.packSuffix}/>
            ) : (
                <Selector changePack={this.changePack} />
            )}
        </div>;
    }
}
